import { Component, AfterViewInit } from '@angular/core';

import { AppState } from '../app.service';


import { BestFirstService } from './home.service';

@Component({
  selector: 'homee',
  providers: [
    BestFirstService
  ],
  styleUrls: [ './home.component.css' ],
  templateUrl: './home.component.html'
})
export class HomeComponent implements AfterViewInit {

  public tempValue: number = -1;
  private bestFirst: Array<string> = [];

  // private road = this.bestFirstService.getRoad();

// ROAD ARREGLO DE NODOS
  private road: any = [
    {
      id: 'A',
      value: 366,
      initial: true
    },
    {
      id: 'B',
      value: 374,
      children: null
    },
    {
     id: 'C',
     value: 329,
     children: null
    },
    {
      id: 'E',
      value: 253,
      children: [
        {
          id: 'G',
          value: 193,
          children: [
            {
              id: 'H',
              value: 98
            }
          ]
        },
        {
          id: 'F',
          value: 178,
          children: [
            {
              id: 'i',
              value: 0,
              end: true
            }
          ]
        }
      ]
    }
  ];


  constructor(private bestFirstService: BestFirstService) {
    console.log(this.road);
  }
// ngOnInit  es un ciclo de vida de angualar el cual se ejecuta antes de la vista
  ngOnInit() {
    // mapeo el objeto
    this.road.map( nodo => {
      console.log(nodo.id);
      if (!nodo.initial) {
        this.calcular(nodo);
      } else {
        this.bestFirst.push(nodo.id);
      }
    });
  }

  ngAfterViewInit() {
    console.log(this.bestFirst, 'camino');
  }

  private calcular(nodo): void {
    if (this.tempValue !== -1) {
      if (this.tempValue < nodo.value && nodo.children !== null) {
          console.log(this.tempValue, 'temp value es menor');
      } else {
        if (nodo.children !== null) {
          console.log(nodo.id, '----> Es menor', nodo);
          this.bestFirst.push(nodo.id);
          this.tempValue = -1;
          nodo.children.map( nodoChildren => {
            if (nodoChildren.value === 0) {
              this.bestFirst.push(nodoChildren.id);
              console.log(nodoChildren, 'hijo', this.tempValue);
            }
            this.calcular(nodoChildren);
          });
        }
      }
    }
    this.tempValue = nodo.value;
  }

}
