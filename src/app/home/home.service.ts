import { Injectable, EventEmitter } from '@angular/core';
@Injectable()
export class BestFirstService {
    constructor() { }

    public getRoad() {
        return [
        {
          id: 'A',
          value: 366,
          initial: true
        },
        {
          id: 'B',
          value: 374,
          initial: false,
          children: null
        },
        {
         id: 'C',
         value: 329,
         initial: false,
         children: null
        },
        {
          id: 'E',
          value: 253,
          initial: false,
          children: [
            {
              id: 'G',
              value: 193,
              initial: false,
              children: [
                {
                  id: 'H',
                  value: 98,
                  initial: false,
                  children: [
                    {
                      id: 'i',
                      value: 0,
                      initial: false,
                      end: true
                    }
                  ]
                }
              ]
            },
            {
              id: 'F',
              value: 178,
              initial: false,
              children: [
                {
                  id: 'i',
                  value: 0,
                  initial: false,
                  end: true
                }
              ]
            }
          ]
        }
      ];
    }

    public getRoadStart() {

    }

}
